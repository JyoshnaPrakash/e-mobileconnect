package com.telecom.mobileconnection.utils;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.telecom.mobileconnection.repository.TrackRepository;
import com.telecom.mobileconnection.repository.UserRepository;

@Component
public class MobileReleaseSchedule {
	@Autowired
	UserRepository userRepository;
	@Autowired
	TrackRepository trackRepository;

	@Scheduled(fixedRate = 20000)
	public void mobileConnectionSchedule() {
		List<Integer> userIdList = trackRepository.getApprovedOrdersList(MobileConnectionContants.APPROVED_STATUS);
		userRepository.updateNewMobileNumberStatus(userIdList, MobileConnectionContants.NEW_CONNECTION_STATUS);
//		List<Integer> userList = userRepository.getApprovedOrdersList(MobileConnectionContants.NEW_CONNECTION_STATUS);
//		sms.sendSms();
	}
}