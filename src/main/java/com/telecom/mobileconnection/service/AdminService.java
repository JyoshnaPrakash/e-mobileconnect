package com.telecom.mobileconnection.service;

import java.util.List;

import com.telecom.mobileconnection.exception.UserRegistrationException;

public interface AdminService {

	public List<Object> getListofConnections(Integer approverId) throws UserRegistrationException;
}
